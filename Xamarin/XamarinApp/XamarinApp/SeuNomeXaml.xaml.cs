﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SeuNomeXaml : ContentPage
	{
		public SeuNomeXaml ()
		{
			InitializeComponent ();
		}
        public void BtnMostrarMensagem_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Info", String.Format("Bem-vindo, {0}", txtNome.Text), "Obrigado!");
        }
        public void BtnObterInfo_Clicked(object sender, EventArgs e)
        {
            this.DisplayAlert("Informções", String.Format("Tipo de dispositivo {0}, Plataforma {1}", Device.Idiom.ToString(), Device.RuntimePlatform.ToString()), "ok");

        }
    }
}