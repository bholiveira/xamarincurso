﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace XamarinApp
{
    public class SeuNome : ContentPage
    {
        public SeuNome()
        {
            Title = "Nome";

            Entry txtNome = new Entry
            {
                HorizontalTextAlignment = TextAlignment.Start,
                FontSize = 20,
                Placeholder = "Seu nome..."
            };
            Button btnMostrarMensagem = new Button
            {
                Text = "Mostrar Mensagem"
            };
            //btnEmitirMensagem.Clicked += BtnEmitirMensagem_Clicked;
            btnMostrarMensagem.Clicked += (sender, args) =>
            {
                this.DisplayAlert("Info", String.Format("Bem-vindo, {0}", txtNome.Text), "Obrigado!");
            };
            Button btnInformacao = new Button {
                Text = "Obiter informações"
            };
            btnInformacao.Clicked += (sender, args) =>
            {
                //Clase device para obter informaçoes
                this.DisplayAlert("Informções", String.Format("Tipo de dispositivo {0}, Plataforma {1}", Device.Idiom.ToString(), Device.RuntimePlatform.ToString()), "ok");
            };

            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.Start,
                Children = {
                    new Label {
                        HorizontalTextAlignment = TextAlignment.Center,
                        Text = "Welcome to Xamarin.Forms!"
                    },
                    new Label
                    {
                        HorizontalTextAlignment = TextAlignment.Start,
                        TextColor = Color.Blue,
                        FontSize =  20,
                        Text = "Digite o seu nome: "
                    }//,
                    //new Entry
                    //{
                    //    HorizontalTextAlignment = TextAlignment.Start,
                    //    FontSize = 20,
                    //    Placeholder = "Seu nome..."
                    //},
                    //new Button
                    //{
                    //    Text = "Mostra Mensagem"
                    //}
                    ,
                    txtNome,
                    btnMostrarMensagem,
                    btnInformacao

                },
                Padding = PagePadding
            };
        }

        public static readonly Thickness PagePadding = GetPagePadding();

        private static Thickness GetPagePadding()
        {
            double topPadding;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    topPadding = 2;
                    break;
                case Device.Android:
                    topPadding = 5;
                    break;
                case Device.UWP:
                    topPadding = 15;
                    break;
                default:
                    topPadding = 15;
                    break;
            }

            return new Thickness(5, topPadding, 5, 0);
        }
        //private void BtnEmitirMensagem_Clicked(object sender, EventArgs e)
        //{
        //    throw new NotImplementedException();
        //}
    }
}