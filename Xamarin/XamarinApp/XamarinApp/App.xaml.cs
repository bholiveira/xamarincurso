﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace XamarinApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var conteudo = new ContentPage
            {
                Title = "Olá Mundo!",
                Content = new StackLayout
                {
                    VerticalOptions = LayoutOptions.Center,
                    Children =
                    {
                        new Label
                        {
                            HorizontalTextAlignment = TextAlignment.Center,
                            Text = "Olá Mundo!"
                        }
                    }
                }
            };
            //MainPage = new NavigationPage(conteudo);
            //MainPage = new NavigationPage(new OlaMundoCs());
            //MainPage = new NavigationPage(new OlaMundoXaml());

            //MainPage = new NavigationPage(new SeuNome());
            //MainPage = new NavigationPage(new SeuNomeXaml());

           MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
