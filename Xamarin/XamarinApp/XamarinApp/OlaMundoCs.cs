﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace XamarinApp
{
    public class OlaMundoCs : ContentPage
    {
        public OlaMundoCs()
        {
            Title = "Olá Mundo!";
            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.Center,
                Children = {
                    new Label {
                        HorizontalTextAlignment = TextAlignment.Center,
                        Text = "Olá Mundo!"
                    }
                }
            };
        }
    }
}