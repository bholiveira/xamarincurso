﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CadastroPessoa : ContentPage
	{
		public CadastroPessoa ()
		{
			InitializeComponent ();
		}

        public void BtnCadastrar_Clicked(object sender, EventArgs e)
        {
            Listagem.pessoas.Add(new Models.Pessoa
            {
                Nome = txtNomePessoa.Text.Trim(),
                Idade = (int)Math.Round(sliIdade.Value),
                Endereco = txtEndreco.Text.Trim()
            });
            Navigation.PopModalAsync();
        }
        public void SliIdade_ValueChanged(object sender,EventArgs e)
        {
            sliIdade.Value = (int)Math.Round(sliIdade.Value);
            lblIdade.Text = sliIdade.Value.ToString() + " ano(s)";
        }
    }
}