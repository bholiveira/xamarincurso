﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.Models;

namespace XamarinApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Listagem : ContentPage
	{
        public static ObservableCollection<Pessoa> pessoas = new ObservableCollection<Pessoa>
        {
            new Pessoa
            {
                Nome = "Mario",
                Idade = 12,
                Endereco = "Avenida teste, 111"
            },
            new Pessoa
            {
                Nome = "Joana",
                Idade = 30,
                Endereco = "Avenida Brasil, 12"
            }
        };
		public Listagem ()
		{
			InitializeComponent ();
            lstPessoa.ItemsSource = pessoas;
		}
        public void BtnAddPessoa_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new CadastroPessoa());
        }

    }
}